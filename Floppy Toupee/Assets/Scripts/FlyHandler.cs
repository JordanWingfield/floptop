﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyHandler : MonoBehaviour {

    public float Velocity;
    public float ForceAmount;
    public Vector2 Target;

    private bool HitTarget = false;

    // Use this for initialization
    void Start () {
        GetTarget();
	}
	
	// Update is called once per frame
	void Update () {
        if (!HitTarget)
        {
            MoveRandomly();
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HitTarget = true;
    }

    private void MoveRandomly()
    {
        var x_min = Target.x - 10;
        var x_max = Target.x + 10;
        var y_min = Target.y - 10;
        var y_max = Target.y + 10;

        var current_pos = new Vector2(transform.position.x, transform.position.y);
        var destination_pos = new Vector2(Random.Range(x_min, x_max), Random.Range(y_min, y_max));
        transform.position = Vector2.MoveTowards(current_pos, destination_pos, 5f * Time.deltaTime);
    }
    
    private void GetTarget()
    {
        Target = GameObject.FindGameObjectWithTag("Head").transform.position;
    }
}
