﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleHandler : MonoBehaviour {

    public float Angle;
    public float BreakThreshold;
    
	void Start () {
        Angle = GetComponent<HingeJoint2D>().jointAngle;
	}
	
	void Update () {
        
        if (!IsFallen())
        {
            UpdateAngle();
        }
        if (IsDead())
        {
            Destroy(this.gameObject);
        }
    }

    private void UpdateAngle()
    {
        Angle = GetComponent<HingeJoint2D>().jointAngle;
    }

    public bool IsFallen()
    {
        if (Mathf.Abs(Angle) > BreakThreshold)
        {
            Destroy(GetComponent<HingeJoint2D>());
            return true;
        }
        return false;
    }

    private bool IsDead()
    {
        if (transform.position.y <= -10)
        {
            return true;
        }
        return false;
    }
}
