﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CandidateButtonManager : MonoBehaviour {

    public GameObject gameData;

    [Space(20)]
    public int candidateIndexNumber;
    public GameObject candidatePanelCover;
    public GameObject checkmarkIcon;
    public Animator candidatePanelAnimator;

    private void Start()
    {
        if (gameData == null)
        {
            Debug.LogError("GameData object not found. Shit's fucked, yo.");

            // When we get the Game Data GameObject working and passing information to the Game Master let's uncomment the next line
            //gameData = GameObject.Find("Game Data");
        }
    }

    public void SelectCandidate()
    {
        checkmarkIcon.SetActive(true);
        candidatePanelCover.SetActive(true);

        // Play checkmark sound effect

        if (gameData != null)
        {
            // Send candidateIndexNumber to gameData
        }

        candidatePanelAnimator.Play("SelectCandidate_Anim");
        StartCoroutine(GoToGameScene(1.5f));
    }

    IEnumerator GoToGameScene(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadSceneAsync(2);
    }
}
