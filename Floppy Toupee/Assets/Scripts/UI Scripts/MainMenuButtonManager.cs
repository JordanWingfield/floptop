﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtonManager : MonoBehaviour {

    public Animator leaderBoardAnimator;
    private bool leaderBoardState = false;


    public void StartCandidateScene()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void LoadLeaderBoards()
    {
        if (leaderBoardState == false)
        {
            Debug.Log("Showing Leader Board");
            ShowLeaderBoards();
        }
        else
        {
            Debug.Log("Hiding Leader Board");
            HideLeaderBoards();
        }
    }

    void ShowLeaderBoards()
    {
        leaderBoardState = true;
        leaderBoardAnimator.Play("LeaderBoardEnter_Anim");
    }

    void HideLeaderBoards()
    {
        leaderBoardState = false;
        leaderBoardAnimator.Play("LeaderBoardExit_Anim");    
    }

    public void QuitGame()
    {
        Application.Quit();
    }


}
