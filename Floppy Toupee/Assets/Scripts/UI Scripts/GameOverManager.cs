﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

    private int gameScore;
    public Text inputName;


	void Start () {
        // Load in current score.
        // Load previous score for comparison.
		// Play load in animation.
	}

    void SubmitScore(string name, int score)
    {
        // Upload score to server using inputName and gameScore
    }
	
	public void ReturnToCandidateSelect()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
