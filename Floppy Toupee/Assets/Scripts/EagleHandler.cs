﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleHandler : MonoBehaviour {

    public float Velocity;
    public float ForceAmount;
    public Vector2 Target;

    public Vector2 StartPosition;
    public Vector2 EndPosition;

    public bool HitTarget = false;

	// Use this for initialization
	void Start () {
        GetTarget();
        SetPositions();
    }
	
	// Update is called once per frame
	void Update () {
        CheckDone();
        if (!HitTarget)
        {
            MoveToTarget();
        }
        else
        {
            MoveToEnd();
        }
    }

    private void CheckDone()
    {
        if (transform.position.x > 3 && transform.position.y > 6)
        {
            Destroy(this.gameObject);
        }
    }

    private void MoveToTarget()
    {
        var current_pos = new Vector2(transform.position.x, transform.position.y);
        transform.position = Vector2.MoveTowards(current_pos, Target, 10f * Time.deltaTime);
    }

    private void MoveToEnd()
    {
        var current_pos = new Vector2(transform.position.x, transform.position.y);
        transform.position = Vector2.MoveTowards(current_pos, EndPosition, 10f * Time.deltaTime);
    }

    private void SetPositions()
    {
        StartPosition = new Vector2(Target.x - 4, Target.y + 6);
        EndPosition = new Vector2(Target.x + 4, Target.y + 6);
        transform.Translate(StartPosition, Space.World);
    }

    private void GetTarget()
    {
        Target = GameObject.FindGameObjectWithTag("Toupee").transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HitTarget = true;
        Destroy(GetComponent<BoxCollider2D>());
        ApplyForce(collision.gameObject.GetComponent<Rigidbody2D>());
    }

    private void ApplyForce(Rigidbody2D body)
    {
        body.AddForce(Target * ForceAmount, ForceMode2D.Impulse);
    }

}
