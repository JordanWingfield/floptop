﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceHandler : MonoBehaviour {

    public float forceAmount = 10f;

    private void OnMouseDown()
    {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * forceAmount, ForceMode2D.Impulse);
        // GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 1) * forceAmount, ForceMode2D.Impulse);
        // GetComponent<Rigidbody2D>()
    }
}
