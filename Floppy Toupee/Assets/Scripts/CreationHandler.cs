﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreationHandler : MonoBehaviour {

    public List<GameObject> Obstacles;
    public float counter = 100;
    public float Maxcounter = 100;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                Instantiate(Obstacles[0], new Vector3(0, 0, 0), new Quaternion());
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(Obstacles[0], new Vector3(0, 0, 0), new Quaternion());
        }
        
        counter -= 1;
        if (counter <= 0)
        {
            var x = Random.Range(-4, 4);
            Instantiate(Obstacles[0], new Vector3(x, -6, 0), new Quaternion());
            counter = Maxcounter;
        }
	}
}
