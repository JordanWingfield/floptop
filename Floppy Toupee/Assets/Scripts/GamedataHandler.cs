﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamedataHandler : MonoBehaviour {

    public int CharacterIndex = 0;
    public SaveData Data;

	void Start ()
    {
        DontDestroyOnLoad(this.gameObject);
        SetupData();
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("Saving data.");
            SaveLoad.Save(Data);
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            SetupData();
        }
    }

    public void Destroy()
    {
        DestroyObject(this.gameObject);
    }

    private void SetupData()
    {
        Debug.Log("Loading data.");
        Data = SaveLoad.Load();
    }
}
