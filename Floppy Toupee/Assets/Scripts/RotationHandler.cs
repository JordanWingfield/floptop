﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHandler : MonoBehaviour {

    public float Angle = 0f;

	void Start () {
        Input.gyro.enabled = true;
	}
	
	void Update ()
    {
        /*foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                ResetOrientation();
            }
        }*/
        //UpdateGravity();
        UpdateRotation();
        SetRotation();
	}

    private void UpdateRotation()
    {
        var input_rotation = Input.gyro.rotationRateUnbiased.z;
        this.transform.Rotate(0, 0, -input_rotation); 
    }

    private void SetRotation()
    {
        this.transform.Rotate(0, 0, Input.GetAxis("Horizontal"));
    }

    private void UpdateGravity()
    {
        Physics2D.gravity = new Vector2(Input.gyro.gravity.x * 9.81f, Input.gyro.gravity.y * 9.81f);
    }

    private void ResetOrientation()
    {
        Angle = GetDeg(Physics2D.gravity.x, Physics2D.gravity.y);
        this.transform.rotation = new Quaternion(0, 0, -Angle, 0);
    }

    public float GetDeg(float x, float y)
    {
        var angle = Mathf.Atan2(x, y) * Mathf.Rad2Deg;
        Debug.Log("Gravity Angle: " + angle.ToString());
        return angle;
    }

}
