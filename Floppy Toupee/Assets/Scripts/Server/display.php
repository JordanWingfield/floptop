<?php
    // Configuration
    $hostname = 'mysql:dbname=scores;host=127.0.0.1';
    $username = 'admin';
    $password = 'password';
    $database = 'scores';
 
    try{
    	$dbh = new PDO($hostname, $username, $password);
    } catch(PDOException $e) {
        echo '<h1>An error has occurred.</h1><pre>', $e->getMessage() ,'</pre>';
    }
 
    $sth = $dbh->query('SELECT * FROM scores ORDER BY score DESC LIMIT 5');
    $sth->setFetchMode(PDO::FETCH_ASSOC);
 
    $result = $sth->fetchAll();
 
    if(count($result) > 0) {
        foreach($result as $r) {
            echo $r['name'], "\t", $r['score'], "\n";
        }
    }
?>