﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// ===================================================================
// Characters are listed in indexed order for organization, and can
// be accessed as follows:
// 0: Trump
// 1: Pence
// 2: Trudeau
// 3: Clinton
// 4: Putin
// 5: Toupee
// ===================================================================

// ===================================================================
// Obstacles are listed in indexed order for organization, and can be
// accessed as follows:
// 0: Shoe
// 1: Eagle
// 2: Fly
// 3: UFO
// ===================================================================

public class MasterController : MonoBehaviour {

    public GameObject Gamedata;

    public List<GameObject> Character_Heads;
    public List<GameObject> Character_Bodies;
    public List<Transform> ChildrenTransforms;
    public GameObject Podium;

    public List<GameObject> Obstacles;
    public float TimeCounter;
    public float MinTime;
    public float MaxTime;

    public Transform canvas;
    public GameObject gameOverScreenPrefab;
    private bool gameOver = false;
    public float Score;
    public float ScoreIncrement;
    public Text podiumBannerText;
    
	public enum GameState {Game_Start, Game_Play, Game_Over, Game_Wait};
    public GameState State;

	void Start () {
        State = GameState.Game_Start;
        if (GetGamedata() == true)
        {
            SetupAssets();
            SetupObstacles();
            SetupScoreboard();
            State = GameState.Game_Play;
        }
        else
        {
            Debug.Log("Error: No object with tag 'Gamedata' found.");
        }
	}

	void Update () {
		if (State == GameState.Game_Play)
        {
            CheckHairState();
            GenerateObstacles();
            UpdateScore();
        }
        else if (State == GameState.Game_Over)
        {
            DisplayGameOver();
        }
        else if (State == GameState.Game_Start)
        {
            Debug.Log("Error: Game has yet to start.");
        }
	}

    private void CheckHairState()
    {
        if (GameObject.FindWithTag("Toupee") == null)
        {
            State = GameState.Game_Over;
            return;
        }
    }

    private bool GetGamedata()
    {
        Gamedata = GameObject.FindWithTag("Gamedata");
        if (Gamedata != null)
        {
            return true;
        }
        return false;
    }

    private void SetupAssets()
    {
        // TODO: replace default value with actual integer from the Gamedata object.
        var character_index = 0;

        Instantiate(Character_Bodies[character_index], ChildrenTransforms[1]);
        Instantiate(Character_Heads[character_index], ChildrenTransforms[0]);
    }

    private void SetupObstacles()
    {
        ResetCounter();
    }

    private void SetupScoreboard()
    {
        Score = 0;
        // TODO: update score on the banner setup to 
        // display.
        DisplayScore();
    }

    private void GenerateObstacles()
    {
        Countdown();
        if (TimeCounter <= 0)
        {
            var obstacle_index = Random.Range(0, 3);
            switch(obstacle_index)
            {
                case 0:
                    CreateShoe();
                    break;
                case 1:
                    CreateEagle();
                    break;
                case 2:
                    CreateFly();
                    break;
                case 3:
                    CreateUFO();
                    break;
                default:
                    Debug.Log("Error: obstacle_index out of bounds for range of objects.");
                    break;
            }
            ResetCounter();
        }
    }

    private void CreateShoe()
    {
        var x = Random.Range(-5, 5);
        Instantiate(Obstacles[0], new Vector3(x, -6, -10), new Quaternion(0, 0, 0, 0));
    }

    private void CreateEagle()
    {
        Instantiate(Obstacles[1], new Vector3(-5, 0, -10), new Quaternion(0, 0, 0, 0));
    }

    private void CreateFly()
    {
        var x = Random.Range(-5, 5);
        var y = Random.Range(-7, 7);
        Instantiate(Obstacles[2], new Vector3(x, y, -10), new Quaternion(0, 0, 0, 0));
    }

    private void CreateUFO()
    {
        var xs = new List<int>();
        xs.Add(-5);
        xs.Add(5);
        var x = Random.Range(0, 1);
        Instantiate(Obstacles[3], new Vector3(x, 0, -10), new Quaternion(0, 0, 0, 0));
    }

    private void Countdown()
    {
        TimeCounter -= 1;
        if (TimeCounter <= 0)
        {
            TimeCounter = 0;
        }
    }

    private void ResetCounter()
    {
        TimeCounter = Random.Range(MinTime, MaxTime);
    }

    private void UpdateScore()
    {
        Score += ScoreIncrement;
        // TODO: Update score on the banner setup to
        // display.
        DisplayScore();
    }

    private void DisplayScore()
    {
        podiumBannerText.text = Score.ToString();
    }

    private void DisplayGameOver()
    {
        if (gameOver == false)
        {
            Debug.Log("Displaying GameOver");
            Instantiate(gameOverScreenPrefab, canvas);
            gameOver = true;
        }
        else
        {
            return;
        }
        
    }
}
